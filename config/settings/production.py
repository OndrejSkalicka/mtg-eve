from .base import *  # noqa

ALLOWED_HOSTS = [env('EVE_ALLOWED_HOSTS', default='*')]
