from .base import *  # noqa

# DEBUG
# ------------------------------------------------------------------------------
DEBUG = env.bool('DJANGO_DEBUG', default=True)
INTERNAL_IPS = ['127.0.0.1', ]
ALLOWED_HOSTS = ['*']

MIDDLEWARE += ['debug_toolbar.middleware.DebugToolbarMiddleware', ]  # noqa
INSTALLED_APPS += ['debug_toolbar', ]  # noqa

DEBUG_TOOLBAR_CONFIG = {
    'DISABLE_PANELS': [
        # had to disable this for performance, see https://github.com/django-crispy-forms/django-crispy-forms/issues/703
        'debug_toolbar.panels.templates.TemplatesPanel',
        # just a pesky one
        'debug_toolbar.panels.redirects.RedirectsPanel',
        # tends to be way too large
        'debug_toolbar.panels.staticfiles.StaticFilesPanel',
    ],
    'SHOW_TEMPLATE_CONTEXT': True,
}
