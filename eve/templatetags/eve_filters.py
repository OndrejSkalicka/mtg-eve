from django import template

register = template.Library()


@register.filter
def percent(value, precision=1):
    if value is None:
        return ''

    f = f'%%.%df' % precision
    return f % (value * 100.0)
