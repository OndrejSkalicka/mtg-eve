from django.contrib import admin

from .models import Deck, Player, Match


class MatchAdmin(admin.ModelAdmin):
    def get_queryset(self, request):
        return super().get_queryset(request).select_related('first_player',
                                                            'second_player',
                                                            'first_deck',
                                                            'second_deck', )


admin.site.register(Deck)
admin.site.register(Player)
admin.site.register(Match, MatchAdmin)
