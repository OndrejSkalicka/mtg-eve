import logging

from django.core.management.base import BaseCommand

from eve.models import Deck

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Recalculate stats of all decks.'

    def handle(self, *args, **options):
        for deck in Deck.objects.prefetch_related('first_matches', 'second_matches').all():
            deck.refresh_wins_losses_draws()

        logger.info("Recalculated all decks' stats.")
