from django.views.generic import ListView

from eve.models import Deck, Match


class DeckList(ListView):
    model = Deck


class MatchList(ListView):
    model = Match
    queryset = Match.objects.select_related('first_deck', 'second_deck', 'first_player', 'second_player')
