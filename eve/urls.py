from django.urls import path
from django.views.generic import TemplateView

from eve.views import DeckList, MatchList

app_name = 'eve'
urlpatterns = [
    path('', TemplateView.as_view(template_name='eve/rules.html'), name='index'),
    path('decks/', DeckList.as_view(), name='decks'),
    path('matches/', MatchList.as_view(), name='matches'),
    path('stats/', TemplateView.as_view(template_name='eve/stats.html'), name='stats'),
]
