# Generated by Django 3.0.2 on 2020-01-06 22:30

import django.utils.datetime_safe
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('eve', '0004_auto_20200106_2231'),
    ]

    operations = [
        migrations.RenameField(
            model_name='match',
            old_name='first_draws',
            new_name='draws',
        ),
        migrations.RemoveField(
            model_name='deck',
            name='draws',
        ),
        migrations.RemoveField(
            model_name='deck',
            name='losses',
        ),
        migrations.RemoveField(
            model_name='deck',
            name='wins',
        ),
        migrations.AddField(
            model_name='deck',
            name='game_draws',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='deck',
            name='game_losses',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='deck',
            name='game_wins',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='deck',
            name='match_losses',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='deck',
            name='match_wins',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='match',
            name='created',
            field=models.DateField(default=django.utils.datetime_safe.date.today),
        ),
    ]
