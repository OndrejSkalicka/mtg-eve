from itertools import chain

from django.db import models
from django.utils.datetime_safe import date


class Deck(models.Model):
    name = models.CharField(max_length=256)
    year = models.PositiveSmallIntegerField(blank=True, null=True)
    aetherhub_id = models.PositiveIntegerField()

    game_wins = models.PositiveIntegerField(default=0)
    game_losses = models.PositiveIntegerField(default=0)
    game_draws = models.PositiveIntegerField(default=0)

    match_wins = models.PositiveIntegerField(default=0)
    match_losses = models.PositiveIntegerField(default=0)

    matches_2_1 = models.PositiveIntegerField(default=0)
    matches_2_0 = models.PositiveIntegerField(default=0)
    matches_1_2 = models.PositiveIntegerField(default=0)
    matches_0_2 = models.PositiveIntegerField(default=0)
    matches_rest = models.PositiveIntegerField(default=0)

    win_loss_ratio = models.FloatField(default=0.0)

    image_url = models.URLField(blank=True, null=True)

    class Meta:
        ordering = ['name']

    def __str__(self):
        if self.year is None:
            return f'{self.name} ({self.game_wins}--{self.game_losses}--{self.game_draws})'
        return f'{self.name} {self.year}  ({self.game_wins}--{self.game_losses}--{self.game_draws})'

    def aetherhub_url(self) -> str:
        return f'https://aetherhub.com/Deck/Public/{self.aetherhub_id}'

    def refresh_wins_losses_draws(self, save=True) -> None:
        matches = list(chain(self.first_matches.all(), self.second_matches.all()))
        self.game_wins = 0
        self.game_losses = 0
        self.game_draws = 0
        self.match_wins = 0
        self.match_losses = 0
        self.matches_2_1 = 0
        self.matches_2_0 = 0
        self.matches_1_2 = 0
        self.matches_0_2 = 0
        self.matches_rest = 0

        for match in matches:
            if match.first_deck == self:
                wins = match.first_wins
                losses = match.second_wins
            else:
                wins = match.second_wins
                losses = match.first_wins

            self.game_wins += wins
            self.game_losses += losses
            self.game_draws += match.draws

            if wins > losses:
                self.match_wins += 1
            elif wins < losses:
                self.match_losses += 1

            if wins == 2 and losses == 0:
                self.matches_2_0 += 1
            elif wins == 2 and losses == 1:
                self.matches_2_1 += 1
            elif wins == 1 and losses == 2:
                self.matches_1_2 += 1
            elif wins == 0 and losses == 2:
                self.matches_0_2 += 1
            else:
                self.matches_rest += 1

        if self.match_wins + self.match_losses + self.game_draws == 0:
            self.win_loss_ratio = 0.0
        else:
            self.win_loss_ratio = (self.match_wins + self.game_draws / 2) / (self.match_wins + self.game_draws + self.match_losses)

        if save:
            self.save()


class Player(models.Model):
    name = models.CharField(max_length=256)

    def __str__(self):
        return self.name


class Match(models.Model):
    first_player = models.ForeignKey(
        Player,
        related_name='+',
        null=True,
        blank=True,
        on_delete=models.SET_NULL, )

    second_player = models.ForeignKey(
        Player,
        related_name='+',
        null=True,
        blank=True,
        on_delete=models.SET_NULL, )

    first_deck = models.ForeignKey(
        Deck,
        related_name='first_matches',
        on_delete=models.PROTECT)

    second_deck = models.ForeignKey(
        Deck,
        related_name='second_matches',
        on_delete=models.PROTECT)

    first_wins = models.PositiveSmallIntegerField(default=0)
    second_wins = models.PositiveSmallIntegerField(default=0)
    draws = models.PositiveSmallIntegerField(default=0)

    created = models.DateField(
        default=date.today,
        editable=True,
    )

    class Meta:
        verbose_name_plural = "matches"

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        super().save(force_insert, force_update, using, update_fields)

        self.first_deck.refresh_wins_losses_draws()
        self.second_deck.refresh_wins_losses_draws()

    def __str__(self):
        return f'{self.first_deck.name} [{self.first_wins} : {self.second_wins}] {self.second_deck.name} ({self.first_player} vs {self.second_player})'
